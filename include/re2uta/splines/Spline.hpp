/*
 * Spline.hpp
 *
 *  Created on: May 30, 2013
 *      Author: andrew.somerville
 */

#pragma once

#include <Eigen/Core>
#include <boost/smart_ptr/shared_ptr.hpp>

namespace re2uta
{

class Spline
{
    public:

        typedef boost::shared_ptr<Spline> Ptr;

        enum EndPointConditionType
        {
            QUADRATIC = 0,
            FIRSTDERIV,
            SECONDDERIV,
        };

        typedef std::pair<EndPointConditionType,double> EndPointCondition;

    public:

        Spline( const Eigen::VectorXd & keyPoints,
                const Eigen::VectorXd & keyTimes,
                const EndPointCondition & startCondition,
                const EndPointCondition & endCondition );

        Eigen::Vector3d eval( double time ); // returns value, first deriv, second deriv

    protected:

        Eigen::VectorXd
        calcKeypointDerivT( const Eigen::VectorXd &   keyTimeValues,
                            const Eigen::VectorXd &   keySpaceValues,
                            const EndPointCondition & beginCondition,
                            const EndPointCondition & endCondition );

        Eigen::VectorXd   m_keySpaceValues;
        Eigen::VectorXd   m_keyTimeValues;
        Eigen::VectorXd   m_keySpaceValuesDerivT;
        EndPointCondition m_startCondition;
        EndPointCondition m_endCondition;
};

}
